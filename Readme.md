# An Introduction to Docker

The goal of this tutorial is to provide a brief introduction to docker. This tutorial assumes that you have no knowledge about docker. 
It is expected that, after completing this tutorial, you will be able to use containerized applications and also use docker for containerizing your applications.

## Main References:
* [Docker CLI Reference](https://docs.docker.com/engine/reference/commandline/docker/)
* [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/)
* [Docker Compose CLI Reference](https://docs.docker.com/compose/reference/overview/)
* [Compose File Reference](https://docs.docker.com/engine/reference/builder/)
